package MyTemplate;

use strict;
use utf8;

use Template;

binmode (STDOUT, ":utf8"); 
binmode (STDERR, ":utf8"); 


sub tt_process
{
  my $tt_name = shift;
  my $vars = shift;

  my $template_dir = "tt";

  my $res = "";
  my $tt = Template->new({
      INCLUDE_PATH => $template_dir,
      INTERPOLATE  => 1,
  }) || die "$Template::ERROR\n";

  $tt->process($tt_name, $vars, \$res)
      || die $tt->error(), "\n";
  return $res;
}

1;
