package MyWikt;

use strict;
use utf8;
use MediaWiki::API;

binmode (STDOUT, ":utf8");
binmode (STDERR, ":utf8");

sub suckup
# эта функция получает строку, скачивает статью с таким заголовком и возвращает её скаляром
{
  my $ttl = shift;
  my $mw = MediaWiki::API->new();
  $mw->{config}->{api_url} = 'https://ru.wiktionary.org/w/api.php';
  my $ctt = $mw->get_page( { title => $ttl } ) -> {'*'};
  return $ctt;
}

sub spitout_onepiece
# эта функция получает заголовок и статью двумя скалярами и запихивает статью в файл
{
  my $ttl = shift;
  my $ctt = shift;
  open F, ">:utf8", $ttl.".txt";
  print F $ctt;
  close F;
}

sub sliceup
# Эта функция получает статью скаляром...
# ... а отдаёт списком скаляров
{
  my $ctt = shift;
  my @strs = split "\n", $ctt;

  my @l = ();
  my $sctn = "";
  foreach my $s (@strs)
  {
    $s .= "\n";
    if ( $s =~ m/{{-([a-z\-]+)-}}/ ) 
    {
      push @l, $sctn;
      $sctn = "";
    }
    $sctn .= $s;
  }
  push @l, $sctn;

  return @l;
}

sub spitout_slices
# эта функция получает список скаляров и распихивает их по файлам
{
  my @l = @_;
  my $c = 0; # наверно, можно обойтись без этого счётчика, но я не знаю, как получить номер элемента списка
  foreach my $section (@l)
  {
    open F, ">:utf8", $c.".txt";
    print ("Открыл файл ".$c.".txt\n");
    print F $section;
    close F;
    $c++;
  }

}


1;
