#!/usr/bin/perl

use strict;
use utf8;

use Template;

binmode (STDOUT, ":utf8"); 
binmode (STDERR, ":utf8"); 


my $hash={var1 => "漢字かな", var2 => 12345, var3=> {subvar => 42}, var4 => ["わさび","mārrutki",] };

my $template = Template->new();

#$template->process('test.tt', $hash)
#    || die $template->error();

print tt_process("test.tt", $hash);


sub tt_process
{
  my $tt_name = shift;
  my $vars = shift;

  my $template_dir = ".";

  my $res = "";
  my $tt = Template->new({
      INCLUDE_PATH => $template_dir,
      INTERPOLATE  => 1,
  }) || die "$Template::ERROR\n";

  $tt->process($tt_name, $vars, \$res)
      || die $tt->error(), "\n";
  return $res;
}

